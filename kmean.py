
#https://github.com/sowmyacr/kmeans_cluster/blob/master/cust_seg_kmeans.ipynb

#https://towardsdatascience.com/clustering-algorithms-for-customer-segmentation-af637c6830ac


from sklearn.cluster import KMeans
#Load the required packages
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

#Plot styling
import seaborn as sns; sns.set()  # for plot styling
#%matplotlib inline

plt.rcParams['figure.figsize'] = (16, 9)
plt.style.use('ggplot')

dataset=pd.read_csv('CLV.csv')

#dataset.head()
print('dataset.head : {}'.format(dataset.head()))

#len(dataset) # of rows
print('len(dataset) # of rows : {}'.format(len(dataset)))

#ldescriptive statistics of the dataset
dataset.describe().transpose() #descriptive statistics of the dataset
print('len(dataset) # of rows : {}'.format(dataset.describe().transpose()))

X=dataset.iloc[:,[0,1]].values

##Fitting kmeans to the dataset
km4=KMeans(n_clusters=4,init='k-means++', max_iter=300, n_init=10, random_state=0)
y_means = km4.fit_predict(X)


#Visualising the clusters for k=4
plt.scatter(X[y_means==0,0],X[y_means==0,1],s=50, c='purple',label='Cluster1')
plt.scatter(X[y_means==1,0],X[y_means==1,1],s=50, c='blue',label='Cluster2')
plt.scatter(X[y_means==2,0],X[y_means==2,1],s=50, c='green',label='Cluster3')
plt.scatter(X[y_means==3,0],X[y_means==3,1],s=50, c='cyan',label='Cluster4')

plt.scatter(km4.cluster_centers_[:,0], km4.cluster_centers_[:,1],s=200,marker='s', c='red', alpha=0.7, label='Centroids')
plt.title('Customer segments')
plt.xlabel('Annual income of customer')
plt.ylabel('Annual spend from customer on site')
plt.legend()
plt.show()