import pandas as pd     
#import numpy as np      # linear algebra
import matplotlib.pyplot as plt
from sklearn import linear_model

#load csv to pandas , pandas is data processing, CSV file I/O
df = pd.read_csv("/Users/onlineasset/Documents/LinearRegresStock/prices.csv", error_bad_lines=False)
df.head()

#plt.xlabel('area(sqr ft)')
plt.ylabel('price(US$)')
plt.scatter(df.area, df.price, color='red', marker='+')

area = 4000

reg = linear_model.LinearRegression()
reg.fit(df[['area']],df.price)

predict = reg.predict([[area]])

print('Price for {} area is : {}'.format(area, predict))

coef = reg.coef_
intercept = reg.intercept_
#print('Coefisient : {}'.format(coef))
#print('Intercept : {}'.format(intercept))

#y = m * area + b
#m = coefisient 
#b = intercept
y = coef * area + intercept
print('coef * area + intercept : {}'.format(y))

plt.xlabel('area', fontsize=20)
plt.ylabel('price', fontsize=20)

#plot lists
plt.scatter(df.area, df.price, color='red', marker='+')

#plot predict
plt.scatter(area, predict, color='green', marker='P')
plt.plot(df.area, reg.predict(df[['area']]), color='blue')

plt.show()

#Load list Text Files
d = pd.read_csv('/Users/onlineasset/Documents/LinearRegresStock/house_area.csv', error_bad_lines=False)
d.head()

p = reg.predict(d)
d['price'] = p

print('Price for {} area is : {}'.format(d, p))

d.to_csv("prediction_result.csv", index=False)