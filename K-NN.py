
#https://medium.com/mmp-li/k-nn-%E0%B8%81%E0%B8%B1%E0%B8%9A-sklearn-machine-learning-101-81350e8402f0?

# K-NN Machine Learning ประเภท Supervised แบบ Classification

import numpy as np
import matplotlib.pyplot as plt
from matplotlib import style

style.use("ggplot")
from sklearn import svm

x = [1, 5, 1.5, 8, 1, 9]
y = [2, 8, 1.8, 8, 0.6, 11]

plt.title("Clf Data")
plt.scatter(x,y)

X = np.array([[1,2],
             [5,8],
             [1.5,1.8],
             [8,8],
             [1,0.6],
             [9,11]])


y = [0,1,2,3,4,5]

clf = svm.SVC(kernel='linear', C = 1.0)
clf.fit(X,y)

#test = np.array([0.58, 0.76])
test = np.array([8,11])

plt.scatter(8,11)

plt.show()

test = test.reshape(1, -1)

print('Clf = {} '.format(clf.predict(test)))