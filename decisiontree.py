#https://medium.com/the-101-story/machine-learning-101-%E0%B8%AA%E0%B8%A3%E0%B9%89%E0%B8%B2%E0%B8%87-ai-%E0%B8%87%E0%B9%88%E0%B8%B2%E0%B8%A2%E0%B9%86-%E0%B8%94%E0%B9%89%E0%B8%A7%E0%B8%A2-python-51df2fec83aa

from sklearn import tree
from sklearn.model_selection import train_test_split
import pandas as pd

data = pd.read_csv('/Users/onlineasset/Documents/DEMO/loan.csv')

print(data.describe())

num_cols = ['income','saving','loaned','lated']

#gini index vs entropy
model = tree.DecisionTreeClassifier(criterion = "gini",max_depth =3)#(criterion = "gini", random_state = 10,max_depth =1 ,min_samples_leaf =1)

print('model.random_state for {} '.format(model.random_state))
print('model.max_depth for {} '.format(model.max_depth))
print('model.min_samples_leaf for {} '.format(model.min_samples_leaf))

feature = data[num_cols]
			
label = data['approve']

var_train,var_test,res_train,res_test  =train_test_split(feature,label,test_size = 0.3)

model.fit(var_train,res_train)
res_pred = model.predict(var_test)
score = model.score(var_test,res_test)

print('score for {} '.format(score))

tree.export_graphviz(model,out_file='tree.dot',feature_names=num_cols,class_names='approve',rounded=True,filled=True)

from IPython.display import Image, display
#Image(tree.plot_tree(model.fit(feature,label)))
#for imageName in tree.export_graphviz(model):
    #print(tree.export_graphviz(model),out_file='tree.dot')
    #display(Image(imageName))

from graphviz import Source
path = '/Users/onlineasset/Documents/DEMO/tree.dot'
s = Source.from_file(path)
s.view()

#from subprocess import call
#call(['dot','-Tpng','tree.dot','-o','tree.png','-Gdpi=600'])
#Image(filename = 'tree.png')

#print(model.predict_proba([[30000, 5000, 0, 0]]))

y_pred = [[30000, 5000, 1, 1]]
print(model.predict([[30000, 5000, 1, 1]]))

score = model.score(var_test,res_test)

#print(model.score(feature,[[30000, 5000, 0, 0]]))
